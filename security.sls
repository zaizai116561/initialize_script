#backup file
{%- set files_backup = ['/etc/login.defs','/etc/init/control-alt-delete.conf','/etc/pam.d/system-auth','/etc/security/limits.conf','/etc/ssh/sshd_config','/etc/profile','/etc/csh.login','/etc/csh.cshrc','/etc/bashrc','/etc/host.conf','/etc/ssh/ssh_banner','/etc/issue','/etc/issue.net'] -%}
{% for file_back in files_backup %}
{{ file_back }}:
  cmd.run:
    - names:
      - cp {{ file_back }} {{ file_back }}.`date '+%m-%d-%Y'`.bak
{% endfor %}

#delete some users
{%- set users = ['adm','lp','shutdown','halt','news','uucp','games','gopher'] -%}
{% for user in users %}
{{ user }}:
  user.absent:
    - name: {{ user }}
    - purge: True
    - force: True
{% endfor %}

del_group:
  group.absent:
    - names:
      - lp
      - news
      - games

#stop some service
{%- set services = ['auditd','ip6tables','netfs','qpidd','postfix']-%}
{% for service in services %}
{{ service }}:
  cmd.run:
    - names:
      - service {{ service }} stop
      - chkconfig {{ service }} off
{% endfor %}

chmod_file:
  cmd.run:
    - cwd: /etc
    - names:
      - chmod 755 /etc
      - chmod 644 /etc/passwd
      - chmod 400 /etc/shadow
      - chmod 644 /etc/group
      - chmod 700 /etc/security
      - chmod 644 /etc/services
      - chmod 600 /etc/xinetd.conf

login.defs:
  file.managed:
    - name: /etc/login.defs
    - source: salt://files/login.defs 

control-alt-delete.conf:
  cmd.run:
    - cwd: /etc/init
    - names:
      - sed -i 's/^start on control-alt-delete/#start on control-alt-delete/g' /etc/init/control-alt-delete.conf
      - sed -i 's/^exec/#exec/g' /etc/init/control-alt-delete.conf

system-auth:
  file.managed:
    - name: /etc/pam.d/system-auth
    - source: salt://files/system-auth
    - template: jinja
    - defaults:
      VALUE5: difok=3 minlen=8 ucredit=-1 lcredit=-1 dcredit=1   

limits.conf:
  file.append:
    - name: /etc/security/limits.conf
    - text:
      - "*    soft    core    0"
      - "*    hard    core    0"
      - "*  soft    nofile  163840"
      - "*  hard    nofile  163840"

sshd_config:
  file.append:
    - name: /etc/ssh/sshd_config
    - text:
      - "Banner  /etc/ssh/ssh_banner"

/etc/vsftpd/vsftpd.conf:
  file.append:
    - text:
      - "ftpd_banner=Authorized users only. All activity will be monitored and reported."
  cmd.run:
    - cwd: /etc
    - names:
      - /etc/init.d/vsftpd restart

{%- set append_files = ['csh.login','csh.cshrc','bashrc'] -%}
{% for append_file in append_files %}
{{ append_file }}:
  file.append:
    - name: /etc/{{ append_file }}
    - text:
      - "umask 027"
  cmd.run:
    - names:
      - source /etc/{{ append_file }}
{% endfor %}

host.conf:
  file.append:
    - name: /etc/host.conf
    - text:
      - "nospoof on"

ssh_banner:
  file.append:
    - name: /etc/ssh/ssh_banner
    - text:
      - "Authorized only. All activity will be monitored and reported."

{%- set issue_files = ['issue','issue.net'] -%}
{% for issue_file in issue_files %}
{{ issue_file }}:
  file.append:
    - name: /etc/{{ issue_file }}
    - text:
      - "Authorized users only. All activity may be monitored and reported."
{% endfor %}


xinetd_restart:
  cmd.run:
    - cwd: /etc
    - names:
      - /etc/init.d/xinetd restart
