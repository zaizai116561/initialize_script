#判断系统版本并yum源初始化
{% if grains['osrelease'] == '6.2' %}
/etc/yum.repos.d/r62.repo:
  file.managed:
    - source: salt://files/r62.repo

{% elif grains['osrelease'] == '6.8' %}
/etc/yum.repos.d/r68.repo:
  file.managed:
    - source: salt://files/r68.repo

{% endif %}
