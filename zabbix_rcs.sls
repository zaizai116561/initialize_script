#安装环境依赖包
pkg_package:
  pkg.installed:
    - names:
      - vim-enhanced
      - lrzsz

/usr/local/zabbix:
  file.recurse:
    - source: salt://files/xmzabbix
    - include_empty: True
    - makedirs: True
    - user: zabbix
    - group: zabbix
    - file_mode: 755
    - dir_mode: 755

/etc/init.d/zabbix_xm_agentd:
  file.managed:
    - source: salt://files/zabbix_xm_agentd
    - mode: 755

zabbix_service:
  cmd.run:
    - names:
      - /sbin/chkconfig --add zabbix_xm_agentd
      - /sbin/chkconfig zabbix_xm_agentd on
      - /etc/init.d/zabbix_xm_agentd restart
