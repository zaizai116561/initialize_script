#backup files
{%- set backup_files = ['/etc/profile','/etc/security/limits.conf','/etc/security/limits.d/90-nproc.conf','/etc/sysctl.conf'] -%}
{% for backup_file in backup_files %}
{{ backup_file }}:
  cmd.run:
    - names:
      - cp {{ backup_file }} {{ backup_file }}.`date '+%m-%d-%Y'`.bak  
{% endfor %}

#内容追加
profile_file:
  file.append:
    - name: /etc/profile
    - text:
      - "ulimit -u 10240"
      - "ulimit -n 655350"
      - "export PRIVATE_IP={{ grains['ip_interfaces']['eth0'][0] }}"
      - "export LOCAL_IP={{ grains['ip_interfaces']['eth0'][0] }}"
      - "TMOUT=180"
      - "umask 027"
  cmd.run:
    - cwd: /etc
    - names:
      - source /etc/profile

#修改参数值
limits.conf_file:
  file.managed:
    - name: /etc/security/limits.conf
    - source: salt://files/limits.conf
    - template: jinja
    - defaults:
      VALUE1: 10240
      VALUE2: 10240
      VALUE3: 655350
      VALUE4: 655350

#以模板定义参数，修改值
90-nproc.conf_file:
  file.managed:
    - name: /etc/security/limits.d/90-nproc.conf
    - source: salt://files/90-nproc.conf
    - template: jinja
    - defaults:
      VALUE6: 655350
      VALUE7: root       soft    nproc     655350

#内容追加
sysctl.conf_file:
  file.append:
    - name: /etc/sysctl.conf
    - text:
      - "net.ipv4.conf.all.accept_redirects = 0"
      - "net.ipv4.tcp_tw_recycle = 1"
      - "net.ipv4.tcp_tw_reuse = 1"
      - "net.ipv4.tcp_max_syn_backlog = 819200"
      - "net.ipv4.tcp_keepalive_time = 600"
      - "net.ipv4.tcp_fin_timeout = 10"
      - "net.core.rmem_max = 16777216"
      - "net.core.wmem_max = 16777216"
      - "net.ipv4.tcp_rmem = 4096 87380 16777216"
      - "net.ipv4.tcp_wmem = 4096 65536 16777216"
      - "net.ipv4.tcp_mem = 16777216 16777216 16777216"
      - "net.core.netdev_max_backlog = 165535"
      - "net.ipv4.tcp_synack_retries = 1"
      - "net.ipv4.tcp_syn_retries = 1"
      - "vm.dirty_background_ratio = 5"
      - "vm.dirty_ratio = 10"
      - "net.ipv4.tcp_max_tw_buckets = 10000"
      - "net.ipv4.tcp_timestamps=0"

#清空crontab
clean_crontab:
  cmd.run:
    - names:
      - crontab -r

#定时清理垃圾邮件
/srv/salt/files/del_maillog.sh:
  cron.present:
    - user: root
    - hour: 2

    
