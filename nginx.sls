#上传文件
/data/soft/openresty.tar.gz:
  file.managed:
    - source: salt://files/openresty.tar.gz
    - require: 
      - file: /data/soft
  cmd.run:
    - cwd: /data/soft
    - names:
      - yum install -y gcc gcc-c++ readline-devel pcre-devel openssl-devel
      - tar -zxvf openresty.tar.gz -C /data

#编译drizzle
/data/openresty/drizzle7-2011.07.21:
  cmd.run:
    - cwd: /data/openresty/drizzle7-2011.07.21
    - names:
      - ./configure --without-server
      - make libdrizzle-1.0
      - make install-libdrizzle-1.0

#编译openresty
/data/openresty/openresty-1.9.15.1:
  cmd.run:
    - cwd: /data/openresty/openresty-1.9.15.1
    - names:
      - mkdir -p /usr/local/openresty
      - ./configure --with-luajit  --prefix=/usr/local/openresty --with-http_ssl_module --with-pcre=../pcre-8.32 --with-zlib=../zlib-1.2.8 --with-openssl=../openssl-1.0.0c --with-libdrizzle=/usr/local --with-http_drizzle_module --with-stream
      - gmake && gmake install
      - mkdir -p /usr/local/openresty/nginx/conf
      - scp -r php/* /usr/local/openresty/nginx/conf/
      - mkdir -p /data/logs/nginx

/data/soft:
  file.directory:
    - makedirs: true
    - user: root
    - group: root

