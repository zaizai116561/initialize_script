#上传安装包并解压
/usr/local/jdk.tar.gz:
  file.managed:
    - source: salt://files/jdk-8u65-linux-x64.tar.gz
  cmd.run:
    - cwd: /usr/local
    - names: 
      - tar -zxvf jdk.tar.gz

#执行完删除压缩包
remove_file:
  cmd.run:
    - names:
      - rm -rf /usr/local/jdk.tar.gz
    - require:
      - file: /usr/local/jdk.tar.gz

#系统添加jdk环境
/etc/profile:
  file.append:
    - text:
      - 'JAVA_HOME=/usr/local/jdk1.8.0_65'
      - 'PATH=$JAVA_HOME/bin:$PATH'
      - 'CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar'
  cmd.run:
    - names:
      - source /etc/profile

