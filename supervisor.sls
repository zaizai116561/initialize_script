#安装依赖库
install_pip:
  pkg.installed:
    - names: 
      - python-pip

#pip方式安装supervisor
supervisor:
  pip.installed:
    - require:
      - pkg: python-pip

#配置工作路径
supervisor-work-path:
  file.directory:
    - names:
      - /data/logs/supervisor
      - /etc/supervisor/conf.d
    - makedirs: True

#配置文件
/etc/supervisor/supervisord.conf:
  file.managed:
    - source: salt://files/conf/supervisord.conf
    - template: jinja
    - require:
      - file: supervisor-work-path

#上传启动文件
/etc/init.d/supervisord:
  file.managed:
    - source: salt://files/script/service.sh
    - mode: 755

#设置服务自启动
reg-supervisor-service:
  cmd.wait:
    - name: chkconfig --add supervisord
    - watch:
      - file: /etc/init.d/supervisord

#启动服务
supervisord:
  service.running:
    - require:
      - cmd: reg-supervisor-service
    - watch:
      - pip: supervisor
      - file: supervisor-work-path




