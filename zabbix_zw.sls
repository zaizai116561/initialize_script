#安装环境依赖包
pkgs:
  pkg.installed:
    - names:
      - vim-enhanced
      - lrzsz

/opt/aspire/product/zabbix:
  file.recurse:
    - source: salt://files/zabbix
    - include_empty: True
    - makedirs: True
    - user: zabbix
    - group: zabbix
    - file_mode: 755
    - dir_mode: 755

#修改文件配置
/opt/aspire/product/zabbix/conf/zabbix_agentd.conf:
  file.append:
    - text:
      - "Hostname={{ grains['ip_interfaces']['eth0'][0] }}"

/etc/init.d/zabbix_asp_agentd:
 file.managed:
    - source: salt://files/zabbix_agentd
    - mode: 755

zabbix_service:
  cmd.run:
    - names:
      - /sbin/chkconfig --add zabbix_asp_agentd
      - /sbin/chkconfig zabbix_asp_agentd on
      - /etc/init.d/zabbix_asp_agentd restart
    - require:
      - file: /opt/aspire/product/zabbix/conf/zabbix_agentd.conf
    
