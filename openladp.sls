#删除用户
del_user:
  user.absent:
    - name: yunwei

#添加用户
add_user:
  user.present:
    - name: ladmin
    - gid: 500
    - password: 'P@ssw0rd01'
  cmd.run:
    - names:
      - chage -M 9999 ladmin

#移除sssd
source_ldap:
  cmd.run:
    - names:
      - yum -y remove sssd
      - yum clean all

#判断系统版本并安装openladp
{% if grains['osmajorrelease']=='6' -%}
source_openldap:
  cmd.run:
    - names:
      - yum -y install openldap-clients nss-pam-ldapd
      - authconfig --enableldap --enablemd5 --enableldapauth --enablelocauthorize --disablefingerprint --ldapserver="172.21.48.153" --ldapbasedn="dc=feinno,dc=com" --kickstar
      - chkconfig nslcd on      
    - require:
      - cmd: source_ldap

#内容追加
/etc/pam.d/system-auth-ac:
  file.append:
    - text:
      - "session    required       pam_mkhomedir.so umask=0077 skel=/etc/skel/"

/etc/pam.d/login:
  file.append:
    - text:
      - "session    required       pam_mkhomedir.so skel=/etc/skel/ umask=0022"

/etc/pam.d/sshd:
  file.append:
    - text:
      - "session    required       pam_mkhomedir.so skel=/etc/skel/ umask=0022"

/etc/pam_ldap.conf:
  file.append:
    - text:
      - "pam_login_attribute uid"
      - "pam_member_attribute member"
      - "pam_groupdn cn='$gcn',ou=Usergroup,dc=feinno,dc=com"

#给admins用户有root权限
/etc/sudoers:
  file.append:
    - text:
      - "%admins    ALL=(ALL)       ALL"

{% elif grains['osmajorrelease']!='6' %}
ldap_source:
  cmd.run:
    - names:
      - yum -y install openldap-clients nss_ldap
      - authconfig --enableldap --enablemd5 --enableldapauth --enablelocauthorize --ldapserver="172.21.48.153" --ldapbasedn="dc=feinno,dc=com" --kickstar
      - chkconfig nscd on
    - require:
      - cmd: source_ldap
  
/etc/pam.d/system-auth-ac:
  file.append:
    -  text:
      - "session    required       pam_mkhomedir.so umask=0077 skel=/etc/skel/"
  
/etc/pam.d/login:
  file.append:
    - text:
      - "session    required       pam_mkhomedir.so skel=/etc/skel/ umask=0022"

/etc/pam.d/sshd:
  file.append:
    - text:
      - "session    required       pam_mkhomedir.so skel=/etc/skel/ umask=0022"

/etc/ldap.conf: 
  file.managed:
    - text:
      - "pam_login_attribute uid"
      - "pam_member_attribute member"
      - "pam_groupdn cn='$gcn',ou=Usergroup,dc=feinno,dc=com"

/etc/sudoers:
  file.append:
    - text:
      - "%admins    ALL=(ALL)       ALL"
{% endif %}

#将系统版本写入文件
/root/temp/check.txt:
  file.append:
    - text:
      - "{{ grains['osmajorrelease'] }}"
    - require:
      - file: /root/temp

#创建目录
/root/temp:
  file.directory:
    - makedirs: true
    - user: root
    - group: root

#disable root romote login
/etc/ssh/sshd_config:
  file.append:
    - text:
      - "PermitRootLogin no"
  cmd.run:
    - names:
      - service sshd restart

