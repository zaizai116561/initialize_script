{%- set backup_files = ['/etc/resolv.conf','/etc/hosts'] -%}
{% for backup_file in backup_files %}
{{ backup_file }}:
  cmd.run:
    - names:
      - cp {{ backup_file }} {{ backup_file }}.`date '+%m-%d-%Y'`.bak
{% endfor %}

#delete dns
resolv.conf_file:
  cmd.run:
    - names:
      - "sed -i '/^nameserver/d' /etc/resolv.conf"

#清空ip+host,只添主机ip+主机名
hosts_file:
  cmd.run:
    - names:
      - "nl /etc/hosts | sed -i '3,$d' /etc/hosts"
  file.managed:
    - name: /etc/hosts
    - source: salt://files/hosts
    - template: jinja
    - defaults:
      VALUE7: {{ grains['ip_interfaces']['eth0'][0] }}
      VALUE8: {{ grains['id']}}

