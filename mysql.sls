#定义变量
{%- set mysql_version = salt['pillar.get']('fae:master:mysql_version','5.5.11-rel20.2-116.Linux.x86_64') -%}
{%- set mysql_path = salt['pillar.get']('ha:fae_mondb:mysql_path','/usr/local/mysql/Percona-Server') -%}
{%- set run_user = salt['pillar.get']('ha:fae_mondb:run_user:name','faemysql') -%}
{%- set run_user_group = salt['pillar.get']('ha:fae_mondb:run_user:group','mysql') -%}
{%- set data_path = salt['pillar.get']('ha:fae_mondb:data_path','/data/faemysql/data') -%}
{%- set tmp_path = salt['pillar.get']('ha:fae_mondb:tmp_path','/usr/local/faemysql/tmp') -%}
{%- set home_path = salt['pillar.get']('ha:fae_mondb:home_path','/usr/local/faemysql') -%}
{%- set use_path = salt['pillar.get']('ha:fae_mondb:use_path','/usr/local') -%}

#安装依赖包
package_pkg:
  pkg.installed:
    - names:
      - libaio

#安装软件
mysql_source:
  file.managed:
    - name: {{ use_path }}/Percona-Server-{{ mysql_version }}.tar.gz
    - source: salt://files/Percona-Server-{{ mysql_version }}.tar.gz    
  cmd.run:
    - cwd: {{ use_path }}
    - names:
      - tar -zxvf  Percona-Server-{{ mysql_version }}.tar.gz 
      - mv Percona-Server-{{ mysql_version }}/* {{ mysql_path }}/
    - require:
      - pkg: package_pkg
      - file: work_path

#软件安装后删除软件包
remove_file:
  cmd.run:
    - cwd: {{ use_path }}
    - names:
      - rm -rf Percona-Server-{{ mysql_version }}.tar.gz
      - rm -rf Percona-Server-{{ mysql_version }}
    - require:
      - file: mysql_source

#创建运行用户
mondb_user:
  user.present:
    - name: {{ run_user }}
    - shell: /sbin/nologin
    - require:
      - group: {{ run_user_group }}
  group.present:
    - name: {{ run_user_group }}

#权限控制
chmod_mysql:
  cmd.run:
    - cwd: {{ use_path }}
    - names: 
      - chown faemysql:faemysql {{ mysql_path }} -R
    - require:
      - file: mysql_source

#配置工作路径
work_path:
  file.directory:
    - names:
      - {{ mysql_path }}
      - {{ data_path }}
      - {{ tmp_path }}
      - {{ use_path }}
      - {{ home_path}}
    - makedirs: True
    - user: {{ run_user }}
    - group: {{ run_user_group }}
    - require:
      - user: mondb_user
      - file: mysql_source

#配置文件
{{ home_path }}/my.cnf:
  file.managed:
    - source: salt://files/my.cnf
    - mysql_path: {{ mysql_path }}
    - data_path: {{ data_path }}
    - tmp_path: {{ tmp_path }}
    - run_user: {{ run_user }}
    - template: jinja
    - require:
      - file: work_path

#上传启动脚本
{{ home_path }}/start.sh:
  file.managed:
    - source: salt://files/start.sh
    - mysql_path: {{ mysql_path }}
    - home_path: {{ home_path }}
    - template: jinja
    - mode: 755
    - require:
      - file: work_path

#mysql初始化
mysql_setting:
  cmd.wait:
    - cwd: {{ mysql_path }}
    - names:
      - ./scripts/mysql_install_db --user={{ run_user }} --defaults-file={{ home_path }}/my.cnf --datadir={{ data_path }}
    - require:
      - file: {{ home_path }}/my.cnf
    - watch:
      - file: mysql_source

#启动MySQL服务
mysql_start:
  cmd.run:
    - names: 
      - {{ home_path }}/start.sh
    - require:
      - cmd: mysql_setting
      












